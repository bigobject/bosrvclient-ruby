Gem::Specification.new do |s|
    s.name        = 'bosrvclient'
    s.version     = '0.1.8'
    s.date        = '2014-10-24'
    s.summary     = 'BigObject service client'
    s.description = 'This is the BigObject service RPC client'
    s.authors     = ['Jeffrey Jen']
    s.email       = 'yihungjen@macrodatalab.com'
    s.files       = ['lib/bosrvclient.rb', *Dir["lib/bosrvclient/*.rb"]]
    s.homepage    = 'https://bitbucket.org/macrodata/bosrvclient-ruby'
    s.license     = 'Apache 2.0'
end
