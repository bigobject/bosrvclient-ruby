require 'openssl'
require 'thrift'
require_relative 'big_object_service'
require 'uri'

module Bosrv
    def Bosrv.connect(addr_info, timeout=nil)
        uri = URI(addr_info)
        builder = uri.scheme == 'bos' ? URI::HTTPS : URI::HTTP
        token = uri.password
        transport = Thrift::HTTPClientTransport.new(
            builder.build({
                :host => uri.host,
                :port => uri.port,
                :path => uri.path
            }),
            { :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE }
        )
        protocol = Thrift::JsonProtocol.new(transport)
        client = Bosrv::BigObjectService::Client.new(protocol)
        transport.open()
        yield [token, client]
    ensure
        #transport.close()
    end
end
