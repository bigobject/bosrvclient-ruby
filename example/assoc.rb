#!/usr/bin/env ruby

require 'bosrvclient'
require 'json'

require_relative 'sql'

if __FILE__ == $0

    addr_info = ENV['BIGOBJECT_URL']

    setup_environ(addr_info)
    populate_steps(addr_info)

    Bosrv.connect addr_info do |token, client|

        build_stmt_qbo = "build association q3(shoes.brand) by users.id from steps"
        start = Time.now.getutc
        client.execute(token, build_stmt_qbo, '', '')
        stop = Time.now.getutc

        puts '--------------------------'
        puts build_stmt_qbo
        puts 'Operation took time: %s' % (stop - start)
        puts '--------------------------', "\n"

        query_stmt = "get 10 freq('Sperry') from q3"
        start = Time.now.getutc
        table = client.execute(token, query_stmt, '', '')
        stop = Time.now.getutc

        puts '--------------------------'
        puts query_stmt
        puts 'Operation took time: %s' % (stop - start)
        puts '--------------------------', "\n"

        eol = limit = 100 # decide the limit of records returned per fetch
        condition = true
        result_table = [] # the destination storage for the result table

        rngspec = Bosrv::RangeSpec.new()
        rngspec.page = limit

        start = Time.now.getutc
        while eol != -1 do
            chunk = JSON.parse(client.cursor_fetch(
                token,
                table,
                rngspec
            ))
            eol, rows = *chunk
            result_table.push(*rows)
        end
        client.cursor_close(token, table)
        stop = Time.now.getutc

        puts "--------------------------"
        puts "Time spent to retrieve from cursor: %s" % (stop - start)
        result_table.each do |r|
            print r, "\n"
        end
        puts "--------------------------"
    end
end
