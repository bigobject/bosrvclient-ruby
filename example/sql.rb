#!/usr/bin/env ruby

require 'bosrvclient'
require 'json'

@users = [
    [1, 'M', 'Taiwan'],
    [2, 'M', 'US'],
    [3, 'F', 'Sleepy Hollow'],
    [4, 'F', 'Taiwan'],
    [5, 'M', 'Japan'],
    [6, 'F', 'United Kindom'],
    [7, 'F', 'Scott'],
    [8, 'F', 'Happy Tree Valley']
]

@shoes = [
    [1, 'Toms'],
    [2, 'Sperry'],
    [3, 'Kate'],
    [4, 'Sofi'],
    [5, 'McClain'],
    [6, 'Cherry'],
    [7, 'Miu Miu'],
    [8, 'Salvatore Ferrogamo'],
]

def setup_environ(addr_info)
    Bosrv.connect addr_info do |token, client|
        sql_stmt = [
            'CREATE TABLE users (id INT, gender STRING, country STRING, KEY (id))',
            'CREATE TABLE shoes (id INT, brand STRING, KEY(id))',
            "CREATE TABLE steps (users.id INT, shoes.id INT, FACT step INT)",
        ]

        sql_stmt.each do |create_stmt|
            client.execute(token, create_stmt, '', '')
        end

        insert_stmt = 'INSERT INTO users VALUES ' + @users.collect {
            |r| "(%d, '%s', '%s')" % r
        } .join(' ')

        client.execute(token, insert_stmt, '', '')

        insert_stmt = 'INSERT INTO shoes VALUES ' + @shoes.collect {
            |r| "(%d, '%s')" % r
        } .join(' ')

        client.execute(token, insert_stmt, '', '')
    end
end

def populate_steps(addr_info, report=true, count=1)
    Bosrv.connect addr_info do |token, client|
        def make_tuple
            return [@users.sample.at(0), @shoes.sample.at(0), [*1..10].sample]
        end

        start = Time.now.getutc

        for _ in 1..count
            step_grp_val = 'INSERT INTO steps VALUES ' + Array.new(10000) {
                "(%d, %d, %d)" % make_tuple
            } .join(' ')

            client.execute(token, step_grp_val, '', '')
        end

        stop = Time.now.getutc

        if report
            puts "--------------------------"
            puts "Insert time for %d values: %s" % [10000 * count, stop - start]
            puts "--------------------------", "\n"
        end
    end
end

if __FILE__ == $0

    addr_info = ENV['BIGOBJECT_URL']

    setup_environ(addr_info)
    populate_steps(addr_info)

    Bosrv.connect addr_info do |token, client|
        select_stmt = 'SELECT SUM(step) FROM steps GROUP BY users.gender, shoes.brand'
        start = Time.now.getutc
        table = client.execute(token, select_stmt, '', '')
        stop = Time.now.getutc

        puts "--------------------------"
        puts select_stmt
        puts "Operation took time: %s" % (stop - start)
        puts "--------------------------", "\n"

        eol = limit = 100 # decide the limit of records returned per fetch
        condition = true
        result_table = [] # the destination storage for the result table

        rngspec = Bosrv::RangeSpec.new()
        rngspec.page = limit

        start = Time.now.getutc
        while eol != -1 do
            chunk = JSON.parse(client.cursor_fetch(
                token,
                table,
                rngspec
            ))
            eol, rows = *chunk
            result_table.push(*rows)
        end
        client.cursor_close(token, table)
        stop = Time.now.getutc

        puts "--------------------------"
        puts "Time spent to retrieve from cursor: %s" % (stop - start)
        result_table.each do |r|
            print r, "\n"
        end
        puts "--------------------------"
    end
end
